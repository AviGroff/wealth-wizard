from queries.client import AccountQueries
from models import AccountIn, AccountOutWithHashedPassword
from fastapi import HTTPException


class DuplicateAccountError(ValueError):
    pass


class AccountsRepo(AccountQueries):
    ACCOUNT_COLLECTION = "accounts"
    COLLECTION = "stocks"

    def create(self, info: AccountIn, hashed_password: str):
        account = info.dict()
        if 15 <= len(account["username"]):
            raise HTTPException(
                status_code=400,
                detail="username length must be less than 15 characters",
            )

        elif len(account["username"]) <= 3:
            raise HTTPException(
                status_code=400,
                detail="username length must be greater than 3 characters",
            )

        elif account["password"] == "":
            raise HTTPException(
                status_code=400,
                detail="username and password may not be empty",
            )

        account["hashed_password"] = hashed_password
        del account["password"]
        account["hashed_password"] = hashed_password
        if self.get(account["username"]) is not None:
            raise DuplicateAccountError
        self.account_collection.insert_one(account)
        account["id"] = str(account["_id"])

        cash = {"total": 10000}
        cash["user_id"] = account["id"]
        cash["symbol"] = "money"
        cash["shares"] = cash["total"]
        cash["name"] = "money"
        self.collection.insert_one(cash)

        return AccountOutWithHashedPassword(**account)

    def get(self, username: str):
        result = self.account_collection.find_one({"username": username})
        if result is None:
            return None
        result["id"] = str(result["_id"])
        return AccountOutWithHashedPassword(**result)

    def get_all(self):
        accounts = []
        for account in self.account_collection.find():
            accounts.append(account["username"])
        return accounts
