import yfinance as yf
from fastapi import HTTPException
from datetime import datetime


def pricing_info(symbol, shares):
    output = {}
    api_stock_info = yf.Ticker(symbol.upper())

    try:
        output["price"] = round(api_stock_info.info["ask"], 2)
        if output["price"] == 0:
            try:
                output["price"] = round(api_stock_info.info["currentPrice"], 2)
            except KeyError:
                raise HTTPException(
                    status_code=404, detail="Invalid Stock Symbol"
                )
    except KeyError:
        raise HTTPException(status_code=404, detail="Invalid Stock Symbol")

    output["name"] = api_stock_info.info["longName"]
    output["total"] = round(shares * output["price"], 2)

    return output


def news_info(symbol):
    api_stock_info = yf.Ticker(symbol.upper())
    return list(
        filter(lambda news: (news.get("thumbnail")), api_stock_info.news)
    )


def performance(symbol, period):
    interval = "1d"
    if period == "1d":
        interval = "30m"
    elif period == "1y":
        interval = "1mo"

    stock = yf.Ticker(symbol.upper())
    list = (
        stock.history(period=period, interval=interval)
        .to_string()
        .split("\n")[2:]
    )
    output = []
    for item in list:
        item = item.split()
        output.append(
            {
                "date": datetime.fromisoformat(item[0] + " " + item[1]),
                "close": float(item[5]),
            }
        )

    return output


def performances(symbols, period):
    interval = "1d"
    if period == "1d":
        interval = "30m"
    elif period == "1y":
        interval = "1mo"

    symbols = symbols.split(",")
    output = []

    for symbol in symbols:
        stock = yf.Ticker(symbol.upper())
        list = (
            stock.history(period=period, interval=interval)
            .to_string()
            .split("\n")[2:]
        )
        for item in list:
            item = item.split()
            output.append(
                {
                    "symbol": symbol,
                    "date": datetime.fromisoformat(item[0] + " " + item[1]),
                    "close": float(item[5]),
                }
            )

    return output
