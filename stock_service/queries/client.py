import os
import pymongo

MONGO_URL = os.environ.get("MONGO_URL", "PROJECT")
client = pymongo.MongoClient(MONGO_URL)


class HistoryQueries:
    @property
    def history_collection(self):
        history_db = client["history"]
        return history_db[self.HISTORY_COLLECTION]


class Queries(HistoryQueries):
    @property
    def collection(self):
        db = client["stocks"]
        return db[self.COLLECTION]


class AccountQueries(Queries):
    @property
    def account_collection(self):
        account_db = client["accounts"]
        return account_db[self.ACCOUNT_COLLECTION]


class CashQueries:
    @property
    def collection(self):
        db = client["cash"]
        return db[self.COLLECTION]
