from fastapi.testclient import TestClient
from main import app
from queries.stocks import StocksQueries
from authenticator import authenticator
from models import StockParams, Stock
from fastapi import HTTPException


client = TestClient(app)


def fake_get_current_account_data():
    return {"id": "fakeuser"}


class FakeStocksQueries:
    def create(self, params: StockParams, user_id="fakeuser") -> Stock:
        stock = params.dict()
        stock["symbol"] = stock["symbol"].upper()
        stock["user_id"] = user_id
        stock["name"] = "AMC Entertainment Holdings, Inc."
        price = 9.00
        stock.update({"price": 9.00, "total": price * stock["shares"]})
        stock["id"] = "64482332fae37cb0433e046c"

        return Stock(**stock)

    def get_all(self, user_id: str) -> list[Stock]:
        stock = {
            "symbol": "AMC",
            "shares": 3,
            "name": "AMC Entertainment Holdings, Inc.",
            "id": "64482332fae37cb0433e046c",
            "user_id": user_id,
        }
        stocks = []
        stock.update(
            {
                "name": "AMC Entertainment Holdings, Inc.",
                "price": 7.52,
                "total": 22.56,
            }
        )
        stock["user_id"] = user_id
        stock["id"] = "64482332fae37cb0433e046c"
        stocks.append(Stock(**stock))

        return stocks

    def buy(self, params: StockParams, user_id: str) -> Stock:
        stock = params.dict()
        stock["symbol"] = stock["symbol"].upper()
        stock["user_id"] = user_id
        price_info = {
            "name": "AMC Entertainment Holdings, Inc.",
            "price": 7.52,
            "total": 22.56,
        }
        result = {"total": 10000}
        user_portfolio = [
            {
                "symbol": "AMC",
                "shares": 3,
                "name": "AMC Entertainment Holdings, Inc.",
                "id": "64482332fae37cb0433e046c",
                "user_id": user_id,
            }
        ]
        for user_stock in user_portfolio:
            if stock["symbol"] == user_stock["symbol"]:
                if result["total"] >= (price_info["total"]):
                    return {
                        "symbol": "AMC",
                        "shares": 3,
                        "name": "AMC Entertainment Holdings, Inc.",
                        "id": "64482332fae37cb0433e046c",
                        "user_id": user_id,
                        "price": 7.52,
                        "total": 22.56,
                    }
                else:
                    raise HTTPException(
                        status_code=402,
                        detail="Not enough funds to purchase this stock",
                    )

        if result["total"] >= (price_info["total"]):
            stock.update(price_info)
            return stock
        else:
            raise HTTPException(
                status_code=402,
                detail="Not enough funds to purchase this stock",
            )

    def sell(self, params: StockParams, user_id: str) -> Stock:
        stock = params.dict()
        stock["symbol"] = stock["symbol"].upper()
        stock["user_id"] = user_id
        user_portfolio = [
            {
                "symbol": "AMC",
                "shares": 3,
                "name": "AMC Entertainment Holdings, Inc.",
                "id": "64482332fae37cb0433e046c",
                "user_id": user_id,
            }
        ]
        # Check if the stock is already in the collection
        # If so then update that stock
        for user_stock in user_portfolio:
            if stock["symbol"] == user_stock["symbol"]:
                if user_stock["shares"] > (stock["shares"]):
                    return {
                        "symbol": "AMC",
                        "shares": 3,
                        "name": "AMC Entertainment Holdings, Inc.",
                        "id": "64482332fae37cb0433e046c",
                        "user_id": user_id,
                        "price": 7.52,
                        "total": 22.56,
                    }

                elif user_stock["shares"] == stock["shares"]:
                    raise HTTPException(
                        status_code=200, detail="stock removed from portfolio"
                    )

                else:
                    raise HTTPException(
                        status_code=400,
                        detail="""You cannot sell more shares
                        than you currently own""",
                    )

        raise HTTPException(
            status_code=404,
            detail="""You cannot sell a stock that
            you don't currently own""",
        )


def test_create_stock():
    # Arrange
    app.dependency_overrides[StocksQueries] = FakeStocksQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    stock = {
        "symbol": "AMC",
        "shares": 3,
    }

    # Act
    res = client.post("/api/stocks", json=stock)
    data = res.json()

    # Assert
    assert data["total"] == 27.00
    assert data["symbol"] == "AMC"
    assert data["shares"] == 3
    assert data["price"] == 9.00
    assert data["name"] == "AMC Entertainment Holdings, Inc."
    assert data["id"] == "64482332fae37cb0433e046c"
    assert data["user_id"] == "fakeuser"

    # Cleanup
    app.dependency_overrides = {}


def test_get_all_stocks():
    # Arrange
    app.dependency_overrides[StocksQueries] = FakeStocksQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    # Act
    res = client.get("/api/stocks")
    data = res.json()

    # Assert
    assert data["stocks"][0]["total"] == 22.56
    assert data["stocks"][0]["symbol"] == "AMC"
    assert data["stocks"][0]["shares"] == 3
    assert data["stocks"][0]["price"] == 7.52
    assert data["stocks"][0]["name"] == "AMC Entertainment Holdings, Inc."
    assert data["stocks"][0]["id"] == "64482332fae37cb0433e046c"
    assert data["stocks"][0]["user_id"] == "fakeuser"

    # Cleanup
    app.dependency_overrides = {}


def test_buy_stock():
    app.dependency_overrides[StocksQueries] = FakeStocksQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    buy = {"symbol": "AMC", "shares": 3}

    # Act
    res = client.post("/api/buy", json=buy)
    data = res.json()
    assert data["total"] == 22.56
    assert data["symbol"] == "AMC"
    assert data["shares"] == 3
    assert data["price"] == 7.52
    assert data["name"] == "AMC Entertainment Holdings, Inc."
    assert data["id"] == "64482332fae37cb0433e046c"
    assert data["user_id"] == "fakeuser"


def test_sell_stock():
    app.dependency_overrides[StocksQueries] = FakeStocksQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    sell = {"symbol": "AMC", "shares": 3}

    # Act
    res = client.post("/api/sell", json=sell)
    data = res.json()

    assert data["detail"] == "stock removed from portfolio"
