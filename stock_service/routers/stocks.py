from fastapi import APIRouter, Depends
from models import (
    Stock,
    StockParams,
    StocksList,
    StockIn,
    StockWithNews,
    NewsList,
    PerformanceList,
    Cash,
    Price,
    PerformancesList,
)
from queries.stocks import StocksQueries
from authenticator import authenticator
from queries.finance_third_party import performance, pricing_info, performances

router = APIRouter()


@router.post("/api/stocks", response_model=Stock)
def create_stock(
    params: StockParams,
    repo: StocksQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.create(params, user_id=account_data["id"])


@router.get("/api/stocks", response_model=StocksList)
def get_all_stocks(
    repo: StocksQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return {"stocks": repo.get_all(user_id=account_data["id"])}


@router.delete("/api/stocks/{stock_id}", response_model=bool)
def delete_stock(
    stock_id: str,
    repo: StocksQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.delete(stock_id, user_id=account_data["id"])


@router.get("/api/stocks/{stock_id}", response_model=StockWithNews)
def get_one_stock(
    stock_id: str,
    repo: StocksQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_one(stock_id, user_id=account_data["id"])


@router.put("/api/stocks/{stock_id}", response_model=Stock)
def update_stock(
    params: StockIn,
    repo: StocksQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.update(params, user_id=account_data["id"])


@router.get("/api/news", response_model=NewsList)
def get_news(
    repo: StocksQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return {"news": repo.get_all_news(user_id=account_data["id"])}


@router.get("/api/performance/", response_model=PerformanceList)
def get_performance(
    symbol: str,
    period: str,
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return {"performance": performance(symbol, period)}


@router.post("/api/cash", response_model=Cash)
def create_cash(
    params: Cash,
    repo: StocksQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.create_money(params, user_id=account_data["id"])


@router.put("/api/cash", response_model=Cash)
def update_cash(
    params: Cash,
    repo: StocksQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.update_money(params, user_id=account_data["id"])


@router.delete("/api/cash/", response_model=bool)
def delete_cash(
    repo: StocksQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.delete_money(user_id=account_data["id"])


@router.get("/api/cash", response_model=Cash)
def get_cash(
    repo: StocksQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_money(user_id=account_data["id"])


@router.get("/api/price/{symbol}", response_model=Price)
def get_price(
    symbol: str,
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    pricing = pricing_info(symbol, 1)
    pricing["symbol"] = symbol.upper()
    return pricing


@router.get("/api/performances/", response_model=PerformancesList)
def get_performances(
    symbol: str,
    period: str,
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return {"performances": performances(symbol, period)}


@router.post("/api/buy/", response_model=Stock)
def perform_buy(
    params: StockParams,
    repo: StocksQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.buy(params, user_id=account_data["id"])


@router.post("/api/sell/", response_model=Stock)
def perform_sell(
    params: StockParams,
    repo: StocksQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.sell(params, user_id=account_data["id"])
