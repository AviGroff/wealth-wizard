from pydantic import BaseModel
from jwtdown_fastapi.authentication import Token
from datetime import datetime


class StockParams(BaseModel):
    symbol: str
    shares: float


class StockIn(BaseModel):
    id: str
    shares: float


class Stock(StockParams):
    name: str
    price: float
    total: float
    id: str
    user_id: str


class StockWithNews(Stock):
    news: list


class StocksList(BaseModel):
    stocks: list[Stock]


class History(Stock):
    transaction_type: str
    date: datetime


class HistoryParams(StockParams):
    transaction_type: str


class HistoryList(BaseModel):
    history: list[History]


class News(BaseModel):
    title: str
    link: str
    thumbnail: str
    publisher: str
    type: str


class NewsList(BaseModel):
    news: list[News]


class Performance(BaseModel):
    date: datetime
    close: float


class PerformanceList(BaseModel):
    performance: list[Performance]


class Performances(BaseModel):
    date: datetime
    close: float
    symbol: str


class PerformancesList(BaseModel):
    performances: list[Performances]


class Cash(BaseModel):
    total: float


class Price(BaseModel):
    symbol: str
    price: float


class AccountOut(BaseModel):
    id: str
    username: str


class AccountIn(BaseModel):
    username: str
    password: str


class AccountOutWithHashedPassword(AccountOut):
    hashed_password: str


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOut
