import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const portfolioApi = createApi({
  reducerPath: "portfolio",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_STOCK_SERVICE_API_HOST,
    credentials: "include",
  }),
  tagTypes: [
    "PortfolioList",
    "NewsList",
    "PerformanceList",
    "PriceList",
    "HistoryList",
    "token",
    "account",
    "CashList",
    "GetOne",
  ],
  endpoints: (builder) => ({
    getAccount: builder.query({
      query: () => "/token",
      transformResponse: (response) => response?.account,
      providesTags: ["Account"],
    }),
   getAccounts: builder.query({
      query: () => "/api/accounts",
    }),
    login: builder.mutation({
      query: (body) => {
        const formData = new FormData();
        formData.append("username", body.username);
        formData.append("password", body.password);

        return {
          url: "/token",
          method: "POST",
          body: formData,
          credentials: "include",
        };
      },
      invalidatesTags: (result) => {
        return (result && ["Account"]) || [];
      },
    }),
    signup: builder.mutation({
      query: (body) => {
        return {
          url: "/api/accounts",
          method: "POST",
          body,
        };
      },
      invalidatesTags: [
        "PortfolioList",
        "NewsList",
        "PerformanceList",
        "PriceList",
        "HistoryList",
        "token",
        "Account",
      ],
    }),
    logout: builder.mutation({
      query: () => ({
        url: "/token",
        method: "DELETE",
      }),
      invalidatesTags: [
        "PortfolioList",
        "NewsList",
        "PerformanceList",
        "PriceList",
        "HistoryList",
        "token",
        "Account",
        "CashList",
      ],
    }),
    getToken: builder.query({
      query: () => ({
        url: "/token",

        credentials: "include",
      }),
      providesTags: ["Token"],
    }),
    getPortfolio: builder.query({
      query: () => "/api/stocks",
      providesTags: ["PortfolioList"],
    }),
    createPortfolio: builder.mutation({
      query: (body) => ({
        url: "/api/stocks",
        method: "POST",
        body,
      }),
      invalidatesTags: ["PortfolioList"],
    }),
    getOnePortfolio: builder.query({
      query: (stock_id) => `/api/stocks/${stock_id}`,
      providesTags: ["GetOne"],
    }),
    updatePortfolio: builder.mutation({
      query: (body) => ({
        url: `/api/stocks/${body.id}`,
        method: "PUT",
        body: body,
      }),
      invalidatesTags: ["PortfolioList"],
    }),
    deletePortfolio: builder.mutation({
      query: (stock_id) => ({
        url: `/api/stocks/${stock_id}`,
        method: "DELETE",
      }),
      invalidatesTags: ["PortfolioList"],
    }),
    getCash: builder.query({
      query: () => "/api/cash",
      providesTags: ["CashList"],
    }),
    updateCash: builder.mutation({
      query: (body) => ({
        url: "/api/cash",
        method: "PUT",
        body: body,
      }),
      invalidatesTags: ["PortfolioList", "Cashlist"],
    }),
    deleteCash: builder.mutation({
      query: () => ({
        url: "/api/cash",
        method: "DELETE",
      }),
      invalidatesTags: (result, error) => [["CashList"]],
    }),
    createCash: builder.mutation({
      query: (body) => ({
        url: "/api/cash",
        method: "POST",
        body,
      }),
      invalidatesTags: ["CashList"],
    }),
    getNews: builder.query({
      query: () => "/api/news",
      providesTags: ["NewsList"],
    }),
    getPerformance: builder.query({
      query: ({ symbol, period }) =>
        `/api/performance/?symbol=${symbol}&period=${period}`,
      providesTags: ["PerformanceList"],
    }),
    getPerformances: builder.query({
      query: ({ symbols, period }) =>
        `/api/performances/?symbol=${symbols}&period=${period}`,
      providesTags: ["PerformancesList"],
    }),
    getOrderHistory: builder.query({
      query: () => "/api/history",
      providesTags: ["HistoryList"],
    }),
    createHistory: builder.mutation({
      query: (body) => ({
        url: "/api/history",
        method: "POST",
        body,
      }),
      invalidatesTags: ["HistoryList"],
    }),
    getPrice: builder.query({
      query: (symbol) => ({
        url: `/api/price/${symbol}`,
      }),
      invalidatesTags: ["PriceList"],
    }),
    createBuy: builder.mutation({
      query: (body) => ({
        url: "/api/buy",
        method: "POST",
        body,
      }),
      invalidatesTags: [
        "PortfolioList",
        "HistoryList",
        "CashList",
        "NewsList",
        "GetOne",
      ],
    }),
    createSell: builder.mutation({
      query: (body) => ({
        url: "/api/sell",
        method: "POST",
        body,
      }),
      invalidatesTags: [
        "PortfolioList",
        "HistoryList",
        "CashList",
        "NewsList",
      ],
    }),
  }),
});

export const {
  useGetNewsQuery,
  useGetPortfolioQuery,
  useGetPerformanceQuery,
  useGetOnePortfolioQuery,
  useLoginMutation,
  useGetOrderHistoryQuery,
  useUpdatePortfolioMutation,
  useCreatePortfolioMutation,
  useGetCashQuery,
  useUpdateCashMutation,
  useGetPriceQuery,
  useCreateHistoryMutation,
  useGetTokenQuery,
  useSignupMutation,
  useLogoutMutation,
  useDeletePortfolioMutation,
  useCreateCashMutation,
  useGetAccountQuery,
  useGetPerformancesQuery,
  useLazyGetPerformancesQuery,
  useCreateBuyMutation,
  useCreateSellMutation,
  useGetAccountsQuery,
} = portfolioApi;
