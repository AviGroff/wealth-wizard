import { useSignupMutation, useGetAccountsQuery } from "../../app/portfolioApi";
import ErrorMessage from "../errorHandling/ErrorMessage";
import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  handlePasswordChange,
  handlePasswordConfirmationChange,
  handleUsernameChange,
  reset,
  errorChange,
} from "../../features/signupSlice";
import { useNavigate, Link } from "react-router-dom";
import { Modal } from "react-bootstrap";
import wizard from "../../media/gifs/wizard_rainbow.gif";
import Sound from "../../media/sounds/glitter_sound.mp4";
import errorSound from "../../media/sounds/ouch.mp3";

const SignupForm = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [signup] = useSignupMutation();
  const { errorMessage, fields } = useSelector((state) => state.signup);
  const [showModal, setModalShow] = useState(false);
  const audio = new Audio(Sound);
  const errorAudio = new Audio(errorSound);
  const { data: accounts, isLoading } = useGetAccountsQuery();

  const handleSubmit = (e) => {
     e.preventDefault();
    if (fields.password !== fields.passwordConfirmation) {
      errorAudio.play();
      dispatch(errorChange("Password does not match confirmation"));
      return;
    }
    else if (fields.username.length > 15){
      errorAudio.play();
      dispatch(errorChange("Username must be less than 15 characters"));
      return;
    }
    else if (fields.username.length < 3) {
      errorAudio.play();
      dispatch(errorChange("Username must be more than 3 characters"))
      return;
    }
    else if (fields.password.length < 1 || fields.passwordConfirmation.length < 1) {
      errorAudio.play();
      dispatch(errorChange("Password must not be empty!"))
      return;
    }
    else if (accounts.accounts.includes(fields.username)) {
      errorAudio.play();
      dispatch(errorChange("Sorry, this username is not available. Please choose another one."))
      return;
    }

    const { username, password } = fields;

    setModalShow(true);
    audio.play();
    setTimeout(() => {
      setModalShow(false);
      signup({
        username,
        password,
      });
      dispatch(reset());
      navigate("/");
    }, 7000);
    };

  if (!isLoading) {
    return (
      <>
        <div className="container">
          <div className="shadow p-4 col-3 position-absolute top-50 start-50 translate-middle bg-white rounded border border-2">
            {false && "HI"}
            <div className="card-body">
              <h5 className="card-title text-primary font-weight-bold">
                SIGN UP
              </h5>
              <hr />
              <form onSubmit={handleSubmit}>
                {errorMessage && <ErrorMessage>{errorMessage}</ErrorMessage>}
                <div className="mb-3">
                  <label htmlFor="Signup__username" className="form-label">
                    Username:
                  </label>
                  <input
                    className="form-control form-control-sm"
                    type={`text`}
                    id="Signup__username"
                    value={fields.username}
                    onChange={(e) =>
                      dispatch(handleUsernameChange(e.target.value))
                    }
                  />
                </div>
                <div className="mb-3">
                  <label htmlFor="Signup__password" className="form-label">
                    Password:
                  </label>
                  <input
                    className="form-control form-control-sm"
                    type={`password`}
                    id="Signup__password"
                    value={fields.password}
                    onChange={(e) =>
                      dispatch(handlePasswordChange(e.target.value))
                    }
                  />
                </div>
                <div className="mb-3">
                  <label
                    htmlFor="Signup__password_confirmation"
                    className="form-label"
                  >
                    Confirm Password:
                  </label>
                  <input
                    className="form-control form-control-sm"
                    type={`password`}
                    id="Signup__password_confirmation"
                    value={fields.passwordConfirmation}
                    onChange={(e) =>
                      dispatch(
                        handlePasswordConfirmationChange(e.target.value)
                      )
                    }
                  />
                </div>
                <div className="text-center">
                  <p>
                    Already have an account?&nbsp;
                    <Link className="text-primary"
                      to={{
                        pathname: "/login",
                        }}>Login!
                    </Link>
                    </p>
                </div>
                <div className="text-center">
                <button type="submit" className="btn btn-primary text-white">
                  SIGN UP
                </button>
                </div>
                <Modal centered show={showModal} className="modal-lg">
                  <Modal.Body>
                    <div className="container align-items-center text-center">
                      <div className="clear"></div>
                      <h3 className="fancy pb-4 mb-4">WELCOME TO</h3>
                      <div className="waviy pt-4">
                        <span style={{ "--i": 1 }}>W</span>
                        <span style={{ "--i": 2 }}>e</span>
                        <span style={{ "--i": 3 }}>a</span>
                        <span style={{ "--i": 4 }}>l</span>
                        <span style={{ "--i": 5 }}>t</span>
                        <span style={{ "--i": 6 }}>h</span>
                        <span style={{ "--i": 7 }}>&nbsp;W</span>
                        <span style={{ "--i": 8 }}>i</span>
                        <span style={{ "--i": 9 }}>z</span>
                        <span style={{ "--i": 10 }}>a</span>
                        <span style={{ "--i": 11 }}>r</span>
                        <span style={{ "--i": 12 }}>d</span>
                      </div>
                      <div className="clear"></div>
                      <div className="text-center">
                        <img
                          className="text-center"
                          src={wizard}
                          alt="wait until the page loads"
                        />
                      </div>
                    </div>
                  </Modal.Body>
                </Modal>
              </form>
            </div>
          </div>
        </div>
      </>
    );
  };
};

export default SignupForm;
