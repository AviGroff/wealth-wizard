import Avi from "../media/pictures/Avi.jpeg";
import Alix from "../media/pictures/Alix.jpeg";
import Jakob from "../media/pictures/Jakob.jpg";
import Matthew from "../media/pictures/Matthew.png";
import MainPic from "../media/pictures/mainpage_pic.png";
import transparent from "../media/pictures/transparent.png";
import ProgressBar from "react-bootstrap/ProgressBar";
import gitlab from "../media/pictures/gitlab.png";
import { Carousel } from "react-bootstrap";
import { Link } from "react-router-dom";
import { useGetAccountQuery } from "../app/portfolioApi";
import React from "react";
import ScrollToTop from "./ScrollToTop";

export default function MainPage() {
  const { data: isLoggedIn, isLoading } = useGetAccountQuery();
  if (isLoading) {
  } else {
    return (
      <>
        <div className="jumbotron bg-light px-4">
          <div className="position-relative">
            {isLoggedIn && (
              <div className="position-absolute top-0 end-0 pe-3 mt-2">
                <h4 className="fst-italic fw-bold text-dark text-wrap text-capitalize">
                  Welcome, {isLoggedIn.username}!
                </h4>
              </div>
            )}
          </div>
          <div className="waviy text-center pt-4">
            <span style={{ "--i": 1 }}>W</span>
            <span style={{ "--i": 2 }}>e</span>
            <span style={{ "--i": 3 }}>a</span>
            <span style={{ "--i": 4 }}>l</span>
            <span style={{ "--i": 5 }}>t</span>
            <span style={{ "--i": 6 }}>h</span>
            <span style={{ "--i": 7 }}>&nbsp;W</span>
            <span style={{ "--i": 8 }}>i</span>
            <span style={{ "--i": 9 }}>z</span>
            <span style={{ "--i": 10 }}>a</span>
            <span style={{ "--i": 11 }}>r</span>
            <span style={{ "--i": 12 }}>d</span>
          </div>
          <div className="clear"></div>
          <div className="container">
            <div className="row gx-4 gx-lg-5 align-items-center my-5">
              <div className="col-lg-7">
                <img
                  className="img-fluid rounded mb-4 mb-lg-0 shadow"
                  src={MainPic}
                  alt="..."
                />
              </div>
              <div className="col-lg-5">
                <Carousel>
                  <Carousel.Item>
                    <img
                      src={transparent}
                      className="d-block w-100"
                      alt="First slide"
                    />
                    <div className="carousel-caption d-flex flex-column justify-content-center h-100">
                      <h3>&nbsp;</h3>
                      <h3 className="text-black fw-light">Welcome to</h3>
                      <h2 className="fst-italic fw-bold text-primary">
                        Wealth Wizard
                      </h2>
                      <h4 className="text-black fw-light">
                        The premiere solution for stocks portfolio
                        management!
                      </h4>
                    </div>
                  </Carousel.Item>
                  <Carousel.Item>
                    <img
                      src={transparent}
                      className="d-block w-100"
                      alt="Second slide"
                    />
                    <div className="carousel-caption d-flex flex-column justify-content-center h-100">
                      <h3>&nbsp;</h3>
                      <h3 className="text-black fw-light">
                        Practice your trading skills with{" "}
                        <em className="fst-italic fw-bold text-primary">
                          Wealth Wizard
                        </em>{" "}
                        !
                      </h3>
                    </div>
                  </Carousel.Item>
                  <Carousel.Item>
                    <img
                      src={transparent}
                      className="d-block w-100"
                      alt="Third slide"
                    />
                    <div className="carousel-caption d-flex flex-column justify-content-center h-100">
                      <h3>&nbsp;</h3>
                      <h4 className="text-black fw-light">
                        Use our stocks portfolio{" "}
                        <em className="fst-italic fw-bold text-primary">
                          performance simulation
                        </em>{" "}
                        !
                      </h4>
                    </div>
                  </Carousel.Item>
                </Carousel>
              </div>
            </div>
          </div>
          <div className="clear"></div>
        </div>
        <div className="jumbotron bg-secondary px-4">
          <div className="clear"></div>
          <div className="container">
            <div className="card mx-4 shadow bg-white pt-4 pb-4">
              <h1 className="text-center text-primary pb-4">
                How it works
              </h1>
              <p className="fs-5 text-center">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="32"
                  height="32"
                  fill="currentColor"
                  className="bi bi-cloud-upload"
                  viewBox="0 0 16 16"
                >
                  <path
                    fillRule="evenodd"
                    d="M4.406 1.342A5.53 5.53 0 0 1 8 0c2.69 0 4.923 2 5.166 4.579C14.758 4.804 16 6.137 16 7.773 16 9.569 14.502 11 12.687 11H10a.5.5 0 0 1 0-1h2.688C13.979 10 15 8.988 15 7.773c0-1.216-1.02-2.228-2.313-2.228h-.5v-.5C12.188 2.825 10.328 1 8 1a4.53 4.53 0 0 0-2.941 1.1c-.757.652-1.153 1.438-1.153 2.055v.448l-.445.049C2.064 4.805 1 5.952 1 7.318 1 8.785 2.23 10 3.781 10H6a.5.5 0 0 1 0 1H3.781C1.708 11 0 9.366 0 7.318c0-1.763 1.266-3.223 2.942-3.593.143-.863.698-1.723 1.464-2.383z"
                  />
                  <path
                    fillRule="evenodd"
                    d="M7.646 4.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1-.708.708L8.5 5.707V14.5a.5.5 0 0 1-1 0V5.707L5.354 7.854a.5.5 0 1 1-.708-.708l3-3z"
                  />
                </svg>
                &nbsp;- Create an account by signing up!
              </p>
              <p className="fs-5 text-center">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="32"
                  height="32"
                  fill="currentColor"
                  className="bi bi-cash"
                  viewBox="0 0 16 16"
                >
                  <path d="M8 10a2 2 0 1 0 0-4 2 2 0 0 0 0 4z" />
                  <path d="M0 4a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v8a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1V4zm3 0a2 2 0 0 1-2 2v4a2 2 0 0 1 2 2h10a2 2 0 0 1 2-2V6a2 2 0 0 1-2-2H3z" />
                </svg>
                &nbsp;- Your account will start with $10,000 by default.
              </p>
              <p className="fs-5 text-center">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="32"
                  height="32"
                  fill="currentColor"
                  className="bi bi-list-task"
                  viewBox="0 0 16 16"
                >
                  <path
                    fillRule="evenodd"
                    d="M2 2.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5V3a.5.5 0 0 0-.5-.5H2zM3 3H2v1h1V3z"
                  />
                  <path d="M5 3.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zM5.5 7a.5.5 0 0 0 0 1h9a.5.5 0 0 0 0-1h-9zm0 4a.5.5 0 0 0 0 1h9a.5.5 0 0 0 0-1h-9z" />
                  <path
                    fillRule="evenodd"
                    d="M1.5 7a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H2a.5.5 0 0 1-.5-.5V7zM2 7h1v1H2V7zm0 3.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5H2zm1 .5H2v1h1v-1z"
                  />
                </svg>
                &nbsp;- Add stocks to your portfolio by buying shares!
              </p>
              <p className="fs-5 text-center">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="32"
                  height="32"
                  fill="currentColor"
                  className="bi bi-tools"
                  viewBox="0 0 16 16"
                >
                  <path d="M1 0 0 1l2.2 3.081a1 1 0 0 0 .815.419h.07a1 1 0 0 1 .708.293l2.675 2.675-2.617 2.654A3.003 3.003 0 0 0 0 13a3 3 0 1 0 5.878-.851l2.654-2.617.968.968-.305.914a1 1 0 0 0 .242 1.023l3.27 3.27a.997.997 0 0 0 1.414 0l1.586-1.586a.997.997 0 0 0 0-1.414l-3.27-3.27a1 1 0 0 0-1.023-.242L10.5 9.5l-.96-.96 2.68-2.643A3.005 3.005 0 0 0 16 3c0-.269-.035-.53-.102-.777l-2.14 2.141L12 4l-.364-1.757L13.777.102a3 3 0 0 0-3.675 3.68L7.462 6.46 4.793 3.793a1 1 0 0 1-.293-.707v-.071a1 1 0 0 0-.419-.814L1 0Zm9.646 10.646a.5.5 0 0 1 .708 0l2.914 2.915a.5.5 0 0 1-.707.707l-2.915-2.914a.5.5 0 0 1 0-.708ZM3 11l.471.242.529.026.287.445.445.287.026.529L5 13l-.242.471-.026.529-.445.287-.287.445-.529.026L3 15l-.471-.242L2 14.732l-.287-.445L1.268 14l-.026-.529L1 13l.242-.471.026-.529.445-.287.287-.445.529-.026L3 11Z" />
                </svg>
                &nbsp;- Practice your trading skills by buying and selling
                stocks.
              </p>
              <p className="fs-5 text-center">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="32"
                  height="32"
                  fill="currentColor"
                  className="bi bi-clipboard-pulse"
                  viewBox="0 0 16 16"
                >
                  <path
                    fillRule="evenodd"
                    d="M10 1.5a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-1Zm-5 0A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5v1A1.5 1.5 0 0 1 9.5 4h-3A1.5 1.5 0 0 1 5 2.5v-1Zm-2 0h1v1H3a1 1 0 0 0-1 1V14a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V3.5a1 1 0 0 0-1-1h-1v-1h1a2 2 0 0 1 2 2V14a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V3.5a2 2 0 0 1 2-2Zm6.979 3.856a.5.5 0 0 0-.968.04L7.92 10.49l-.94-3.135a.5.5 0 0 0-.895-.133L4.232 10H3.5a.5.5 0 0 0 0 1h1a.5.5 0 0 0 .416-.223l1.41-2.115 1.195 3.982a.5.5 0 0 0 .968-.04L9.58 7.51l.94 3.135A.5.5 0 0 0 11 11h1.5a.5.5 0 0 0 0-1h-1.128L9.979 5.356Z"
                  />
                </svg>
                &nbsp;- Check out your global performances on the{" "}
                <em className="fst-italic">Statistics page</em>!
              </p>
              <p className="fs-5 text-center">
                <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" className="bi bi-exclamation-triangle" viewBox="0 0 16 16">
                  <path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/>
                  <path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/>
                </svg>
                &nbsp; - No selling fees were considered when virtually selling stocks.
              </p>
            </div>
          </div>
          <div className="clear"></div>
        </div>
        <div className="jumbotron bg-light px-4">
          <div className="container">
            <div className="clear"></div>
            <h1 className="text-center text-primary pb-4">
              Application Development
            </h1>
            <p className="fs-5">
              <em className="fst-italic fw-bold text-primary">
                Wealth Wizard
              </em>{" "}
              was developed during the final module of the Hack Reactor
              19-week intensive bootcamp program and is intended for
              educational purposes only, and no actual transactions are
              made.
            </p>
            <p className="fs-5">
              <em className="fst-italic fw-bold text-primary">
                Wealth Wizard
              </em>{" "}
              is a FastAPI web appliction using three different collections
              with a MongoDB database. The interactice Web interface is
              built using React/Redux components importing Bootstrap and
              CSS files.
            </p>
            <div className="d-flex justify-content-between mt-4">
              <h5>JAVASCRIPT </h5>
              <h5>72.4%</h5>
            </div>
            <ProgressBar
              className="mb-2"
              variant="primary bg-primary"
              animated
              now={72.4}
            />
            <div className="d-flex justify-content-between">
              <h5>PYTHON </h5>
              <h5>25.0%</h5>
            </div>
            <ProgressBar
              className="mb-2"
              variant="primary bg-primary"
              animated
              now={25.0}
            />
            <div className="d-flex justify-content-between">
              <h5>SCSS</h5>
              <h5>1.5%</h5>
            </div>
            <ProgressBar
              className="mb-2"
              variant="primary bg-primary"
              animated
              now={1.5}
            />
            <div className="d-flex justify-content-between">
              <h5>HTML</h5>
              <h5>0.9%</h5>
            </div>
            <ProgressBar
              className="mb-2"
              variant="primary bg-primary"
              animated
              now={0.9}
            />
            <div className="d-flex justify-content-between">
              <h5>DOCKERFILE</h5>
              <h5>0.2%</h5>
            </div>
            <ProgressBar
              className="mb-2"
              variant="primary bg-primary"
              animated
              now={0.2}
            />
          </div>
          <div className="clear"></div>
        </div>
        <div className="jumbotron bg-secondary px-4">
          <h1 className="text-center text-primary pt-4 pb-4">Our Team</h1>
          <div className="row align-items-start">
            <div className="col">
              <div className="card px-2 shadow bg-white">
                <img alt="Avi"
                  className="rounded-circle mx-auto mt-2"
                  width="140"
                  height="140"
                  src={Avi}
                />
                <h4 className="mx-auto mt-2">Avi Groff</h4>
                <div className="m-3">
                  <p className="card-text">
                    Full-stack developer that is passionate about creating
                    efficient, clean code. I write my code with robust and
                    scalable solutions in mind.
                  </p>
                </div>
                <p className="text-center">
                  <Link
                    target={"_blank"}
                    to="https://www.linkedin.com/in/avi-groff-449795153/"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="32"
                      height="32"
                      fill="currentColor"
                      className="bi bi-linkedin mx-2"
                      viewBox="0 0 16 16"
                    >
                      <path d="M0 1.146C0 .513.526 0 1.175 0h13.65C15.474 0 16 .513 16 1.146v13.708c0 .633-.526 1.146-1.175 1.146H1.175C.526 16 0 15.487 0 14.854V1.146zm4.943 12.248V6.169H2.542v7.225h2.401zm-1.2-8.212c.837 0 1.358-.554 1.358-1.248-.015-.709-.52-1.248-1.342-1.248-.822 0-1.359.54-1.359 1.248 0 .694.521 1.248 1.327 1.248h.016zm4.908 8.212V9.359c0-.216.016-.432.08-.586.173-.431.568-.878 1.232-.878.869 0 1.216.662 1.216 1.634v3.865h2.401V9.25c0-2.22-1.184-3.252-2.764-3.252-1.274 0-1.845.7-2.165 1.193v.025h-.016a5.54 5.54 0 0 1 .016-.025V6.169h-2.4c.03.678 0 7.225 0 7.225h2.4z" />
                    </svg>
                  </Link>
                  <Link
                    target={"_blank"}
                    to="https://gitlab.com/AviGroff"
                  >
                    <img
                      alt="gitlab logo"
                      className="img-fluid mx-2 gitlab"
                      src={gitlab}
                      width="46"
                      height="46"
                    />
                  </Link>
                </p>
              </div>
            </div>
            <div className="col">
              <div className="card shadow bg-white">
                <img
                  alt="Alix"
                  className="rounded-circle mx-auto mt-2"
                  width="140"
                  height="140"
                  src={Alix}
                />
                <h4 className="mx-auto mt-2">Alix de Pannemaecker</h4>
                <div className="m-3">
                  <p className="card-text">
                    Full-stack software engineer with an initial background
                    as a Professional Mechanical Engineer. Passionate about
                    Engineering, I love creating solutions to complex
                    problems.
                  </p>
                </div>
                <p className="text-center">
                  <Link
                    target={"_blank"}
                    to="https://www.linkedin.com/in/alix-de-pannemaecker/"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="32"
                      height="32"
                      fill="currentColor"
                      className="bi bi-linkedin mx-2"
                      viewBox="0 0 16 16"
                    >
                      <path d="M0 1.146C0 .513.526 0 1.175 0h13.65C15.474 0 16 .513 16 1.146v13.708c0 .633-.526 1.146-1.175 1.146H1.175C.526 16 0 15.487 0 14.854V1.146zm4.943 12.248V6.169H2.542v7.225h2.401zm-1.2-8.212c.837 0 1.358-.554 1.358-1.248-.015-.709-.52-1.248-1.342-1.248-.822 0-1.359.54-1.359 1.248 0 .694.521 1.248 1.327 1.248h.016zm4.908 8.212V9.359c0-.216.016-.432.08-.586.173-.431.568-.878 1.232-.878.869 0 1.216.662 1.216 1.634v3.865h2.401V9.25c0-2.22-1.184-3.252-2.764-3.252-1.274 0-1.845.7-2.165 1.193v.025h-.016a5.54 5.54 0 0 1 .016-.025V6.169h-2.4c.03.678 0 7.225 0 7.225h2.4z" />
                    </svg>
                  </Link>
                  <Link
                    target={"_blank"}
                    to="https://gitlab.com/Alix-de-Pannemaecker"
                  >
                    <img
                      alt="gitlab logo"
                      className="img-fluid mx-2 gitlab"
                      src={gitlab}
                      width="46"
                      height="46"
                    />
                  </Link>
                </p>
              </div>
            </div>
            <div className="col ">
              <div className="card px-2 shadow bg-white">
                <img
                  alt="Jakob"
                  className="rounded-circle mx-auto mt-2"
                  width="140"
                  height="140"
                  src={Jakob}
                />
                <h4 className="mx-auto mt-2">Jakob Schweter</h4>
                <div className="m-3">
                  <p className="card-text">
                    As a software engineer I am passionate about using
                    technology to build innovative solutions.
                  </p>
                </div>
                <p className="text-center">
                  <Link
                    target={"_blank"}
                    to="https://www.linkedin.com/in/jakobschwet/"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="32"
                      height="32"
                      fill="currentColor"
                      className="bi bi-linkedin mx-2"
                      viewBox="0 0 16 16"
                    >
                      <path d="M0 1.146C0 .513.526 0 1.175 0h13.65C15.474 0 16 .513 16 1.146v13.708c0 .633-.526 1.146-1.175 1.146H1.175C.526 16 0 15.487 0 14.854V1.146zm4.943 12.248V6.169H2.542v7.225h2.401zm-1.2-8.212c.837 0 1.358-.554 1.358-1.248-.015-.709-.52-1.248-1.342-1.248-.822 0-1.359.54-1.359 1.248 0 .694.521 1.248 1.327 1.248h.016zm4.908 8.212V9.359c0-.216.016-.432.08-.586.173-.431.568-.878 1.232-.878.869 0 1.216.662 1.216 1.634v3.865h2.401V9.25c0-2.22-1.184-3.252-2.764-3.252-1.274 0-1.845.7-2.165 1.193v.025h-.016a5.54 5.54 0 0 1 .016-.025V6.169h-2.4c.03.678 0 7.225 0 7.225h2.4z" />
                    </svg>
                  </Link>
                  <Link
                    target={"_blank"}
                    to="https://gitlab.com/jako1001"
                  >
                    <img
                      alt="gitlab logo"
                      className="img-fluid mx-2 gitlab"
                      src={gitlab}
                      width="46"
                      height="46"
                    />
                  </Link>
                </p>
              </div>
            </div>
            <div className="col">
              <div className="card shadow bg-white">
                <img
                  alt="Matthew"
                  className="rounded-circle mx-auto mt-2"
                  width="140"
                  height="140"
                  src={Matthew}
                />
                <h4 className="mx-auto">Matthew Yoakum</h4>
                <div className="m-3">
                  <p className="card-text">
                    Full stack software engineer. I like to build useful
                    and beautiful things with creative, brilliant, and kind
                    people.
                  </p>
                </div>
                <p className="text-center">
                  <Link
                    target={"_blank"}
                    to="https://www.linkedin.com/in/matthewjyoakum/"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="32"
                      height="32"
                      fill="currentColor"
                      className="bi bi-linkedin mx-2"
                      viewBox="0 0 16 16"
                    >
                      <path d="M0 1.146C0 .513.526 0 1.175 0h13.65C15.474 0 16 .513 16 1.146v13.708c0 .633-.526 1.146-1.175 1.146H1.175C.526 16 0 15.487 0 14.854V1.146zm4.943 12.248V6.169H2.542v7.225h2.401zm-1.2-8.212c.837 0 1.358-.554 1.358-1.248-.015-.709-.52-1.248-1.342-1.248-.822 0-1.359.54-1.359 1.248 0 .694.521 1.248 1.327 1.248h.016zm4.908 8.212V9.359c0-.216.016-.432.08-.586.173-.431.568-.878 1.232-.878.869 0 1.216.662 1.216 1.634v3.865h2.401V9.25c0-2.22-1.184-3.252-2.764-3.252-1.274 0-1.845.7-2.165 1.193v.025h-.016a5.54 5.54 0 0 1 .016-.025V6.169h-2.4c.03.678 0 7.225 0 7.225h2.4z" />
                    </svg>
                  </Link>
                  <Link
                    target={"_blank"}
                    to="https://gitlab.com/matt.yoakum43"
                  >
                    <img
                      className="img-fluid mx-2 gitlab"
                      src={gitlab}
                      width="46"
                      height="46"
                      alt="gitlab"
                    />
                  </Link>
                </p>
              </div>
            </div>
            <div className="clear"></div>
          </div>
        </div>
        <div className="jumbotron bg-light px-4">
          <div className="container">
            <h1 className="text-center text-primary pt-4 pb-4">
              Important Legal Disclaimer
            </h1>
            <p className="fs-5">
              Yahoo!, Y!Finance, and Yahoo! finance are registered
              trademarks of Yahoo, Inc. yfinance is not affiliated,
              endorsed, or vetted by Yahoo, Inc. It's an open-source tool
              that uses Yahoo's publicly available APIs, and is intended
              for research and educational purposes. You should refer to
              Yahoo!'s terms of use (
              <Link target={"_blank"} to="https://legal.yahoo.com/us/en/yahoo/terms/product-atos/apiforydn/index.html" >
                here
              </Link>
              ,{" "}
              <Link target={"_blank"} to="https://legal.yahoo.com/us/en/yahoo/terms/otos/index.html">
                here
              </Link>
              , and{" "}
              <Link target={"_blank"} to="https://policies.yahoo.com/us/en/yahoo/terms/index.htm">
                here
              </Link>
              ) for details on your rights to use the actual data
              downloaded. Remember - the Yahoo! finance API is intended for
              personal use only. For more information, refer to{" "}
              <Link target={"_blank"} to="https://pypi.org/project/yfinance/">yfinance</Link>
              .
            </p>
            <p className="fs-5">
              Our{" "}
              <em className="fst-italic fw-bold text-primary">
                Wealth Wizard
              </em>{" "}
              web application was developed during the final module of the
              Hack Reactor 19-week intensive bootcamp program.{" "}
              <em className="fst-italic fw-bold text-primary">
                Wealth Wizard
              </em>{" "}
              is intended for educational purposes only, and no actual
              transactions are made.
            </p>
          </div>
          <ScrollToTop />
          <div className="clear"></div>
        </div>
      </>
    );
  };
}
