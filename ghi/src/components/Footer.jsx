import React from "react";

function Footer() {
  return (
    <>
      <footer className="mt-auto bg-info text-white fixed-bottom">
        <div className="container text-center">
          <div className="row pt-0 align-items-center mt-2 mb-2">
            <p className="m-0 p-0">Made with ❤️ using yfinance 0.2.18</p>
            <p className="m-0 p-0">Our app is intended for educational purposes only - No actual transactions are made </p>
          </div>
        </div>
      </footer>
    </>
  );
}

export default Footer;
