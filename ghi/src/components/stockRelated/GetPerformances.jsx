import { useState } from "react";
import { useGetPerformancesQuery, useGetPerformanceQuery } from "../../app/portfolioApi";
import Chart2 from "./graphs/LineChart";

function GetPerformances({ stocks }) {
  let symbols = [];
  for (let stock of stocks) {
    symbols.push(stock.symbol);
  }

  let shares = [];
  for (let stock of stocks) {
    shares.push(stock.shares);
  }

  let obj = symbols.map((symbol, index) => {
    return Object.assign(
      {},
      {
        symbol,
        share: shares[index],
      }
    );
  });

  const { data: marketDay, isLoading: isLoadingMarketDay } =
    useGetPerformanceQuery({ symbol: "SPY", period: "1d" });
  const { data: marketMonth, isLoading: isLoadingMarketMonth } =
    useGetPerformanceQuery({ symbol: "SPY", period: "1mo" });
  const { data: marketYear, isLoading: isLoadingMarketYear } =
    useGetPerformanceQuery({ symbol: "SPY", period: "1y" });

  const { data: performancesDay, isLoading: isLoadingDay } =
    useGetPerformancesQuery({ symbols: symbols, period: "1d" });
  const { data: performancesMonth, isLoading: isLoadingMonth } =
    useGetPerformancesQuery({ symbols: symbols, period: "1mo" });
  const { data: performancesYear, isLoading: isLoadingYear } =
    useGetPerformancesQuery({ symbols: symbols, period: "1y" });

  const [showDay, setShowDay] = useState(true);
  const [showMonth, setShowMonth] = useState(true);
  const [showYear, setShowYear] = useState(true);

  const hideDay = () => {
    if (showDay === true) {
      return setShowDay(false);
    } else {
      return setShowDay(true);
    }
  };

  const hideMonth = () => {
    if (showMonth === true) {
      return setShowMonth(false);
    } else {
      return setShowMonth(true);
    }
  };

  const hideYear = () => {
    if (showYear === true) {
      return setShowYear(false);
    } else {
      return setShowYear(true);
    }
  };

  const arrowUp = (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="32"
      height="32"
      fill="currentColor"
      className="bi bi-arrow-bar-up"
      viewBox="0 0 16 16"
    >
      <path
        fillRule="evenodd"
        d="M8 10a.5.5 0 0 0 .5-.5V3.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 3.707V9.5a.5.5 0 0 0 .5.5zm-7 2.5a.5.5 0 0 1 .5-.5h13a.5.5 0 0 1 0 1h-13a.5.5 0 0 1-.5-.5z"
      />
    </svg>
  );

  const arrowDown = (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="32"
      height="32"
      fill="currentColor"
      className="bi bi-arrow-bar-down"
      viewBox="0 0 16 16"
    >
      <path
        fillRule="evenodd"
        d="M1 3.5a.5.5 0 0 1 .5-.5h13a.5.5 0 0 1 0 1h-13a.5.5 0 0 1-.5-.5zM8 6a.5.5 0 0 1 .5.5v5.793l2.146-2.147a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 0 1 .708-.708L7.5 12.293V6.5A.5.5 0 0 1 8 6z"
      />
    </svg>
  );

  if (!isLoadingDay && !isLoadingMonth && !isLoadingYear && !isLoadingMarketDay && !isLoadingMarketMonth && !isLoadingMarketYear) {

    let marketCopyDay = JSON.parse(JSON.stringify(marketDay));
    let marketCopyMonth = JSON.parse(JSON.stringify(marketMonth));
    let marketCopyYear = JSON.parse(JSON.stringify(marketYear));

    let perfMarketDay = marketCopyDay.performance;
    let GainOrLossInPercentsMarketDay = Math.round((((100 * perfMarketDay[perfMarketDay.length - 1].close) / perfMarketDay[0].close) - 100 ) * 100) / 100;
    const arrowDirectionMarketDay = GainOrLossInPercentsMarketDay > 0 ? arrowUp : arrowDown;
    const colorCodeMarketDay = GainOrLossInPercentsMarketDay > 0 ? "primary" : "danger";

    let perfMarketMonth = marketCopyMonth.performance;
    let GainOrLossInPercentsMarketMonth = Math.round((((100 * perfMarketMonth[perfMarketMonth.length - 1].close) / perfMarketMonth[0].close) - 100 ) * 100) / 100;
    const arrowDirectionMarketMonth = GainOrLossInPercentsMarketMonth > 0 ? arrowUp : arrowDown;
    const colorCodeMarketMonth = GainOrLossInPercentsMarketMonth > 0 ? "primary" : "danger";

    let perfMarketYear = marketCopyYear.performance;
    let GainOrLossInPercentsMarketYear = Math.round((((100 * perfMarketYear[perfMarketYear.length - 1].close) / perfMarketYear[0].close) - 100 ) * 100) / 100;
    const arrowDirectionMarketYear = GainOrLossInPercentsMarketYear > 0 ? arrowUp : arrowDown;
    const colorCodeMarketYear = GainOrLossInPercentsMarketYear > 0 ? "primary" : "danger";

    let performancesCopyDay = JSON.parse(JSON.stringify(performancesDay));
    performancesCopyDay = performancesCopyDay.performances.map((perf) => {
      for (let thing of obj) {
        if (perf.symbol === thing.symbol) {
          perf["close"] = perf["close"] * thing.share;
        }
      }
      return perf;
    });

    let newPerformancesDay = [];
    for (let i = 0; i < performancesCopyDay.length; i++) {
      let dateDayIndex = newPerformancesDay.findIndex(
        (perf) => perf.date === performancesCopyDay[i].date
      );

      if (dateDayIndex < 0) {
        newPerformancesDay.push({
          close: performancesCopyDay[i].close,
          date: performancesCopyDay[i].date,
        });
      } else {
        newPerformancesDay[dateDayIndex].close +=
          performancesCopyDay[i].close;
      }
    }

    let GainOrLossInPercentsDay = Math.round((((100 * newPerformancesDay[newPerformancesDay.length - 1].close) / newPerformancesDay[0].close) -100 ) * 100) / 100;
    const arrowDirectionDay = GainOrLossInPercentsDay > 0 ? arrowUp : arrowDown;
    const colorCodeDay = GainOrLossInPercentsDay > 0 ? "primary" : "danger";

    let minDay = Number.POSITIVE_INFINITY;
    let maxDay = Number.NEGATIVE_INFINITY;
    let tmpDay;
    for (let i = newPerformancesDay.length - 1; i >= 0; i--) {
      tmpDay = newPerformancesDay[i].close;
      if (tmpDay < minDay) minDay = tmpDay;
      if (tmpDay > maxDay) maxDay = tmpDay;
    }
    minDay = Math.round(minDay * 100) / 100;
    maxDay = Math.round(maxDay * 100) / 100;

    let performancesCopyMonth = JSON.parse(
      JSON.stringify(performancesMonth)
    );
    performancesCopyMonth = performancesCopyMonth.performances.map(
      (perf) => {
        for (let thing of obj) {
          if (perf.symbol === thing.symbol) {
            perf["close"] = perf["close"] * thing.share;
          }
        }
        return perf;
      }
    );

    let newPerformancesMonth = [];
    for (let i = 0; i < performancesCopyMonth.length; i++) {
      let dateMonthIndex = newPerformancesMonth.findIndex(
        (perf) => perf.date === performancesCopyMonth[i].date
      );

      if (dateMonthIndex < 0) {
        newPerformancesMonth.push({
          close: performancesCopyMonth[i].close,
          date: performancesCopyMonth[i].date,
        });
      } else {
        newPerformancesMonth[dateMonthIndex].close +=
          performancesCopyMonth[i].close;
      }
    }

    let GainOrLossInPercentsMonth = Math.round((((100 * newPerformancesMonth[newPerformancesMonth.length - 1].close) / newPerformancesMonth[0].close) -100 ) * 100) / 100;
    const arrowDirectionMonth = GainOrLossInPercentsMonth > 0 ? arrowUp : arrowDown;
    const colorCodeMonth = GainOrLossInPercentsMonth > 0 ? "primary" : "danger";

    let minMonth = Number.POSITIVE_INFINITY;
    let maxMonth = Number.NEGATIVE_INFINITY;
    let tmpMonth;
    for (let i = newPerformancesMonth.length - 1; i >= 0; i--) {
      tmpMonth = newPerformancesMonth[i].close;
      if (tmpMonth < minMonth) minMonth = tmpMonth;
      if (tmpMonth > maxMonth) maxMonth = tmpMonth;
    }
    minMonth = Math.round(minMonth * 100) / 100;
    maxMonth = Math.round(maxMonth * 100) / 100;

    let performancesCopyYear = JSON.parse(
      JSON.stringify(performancesYear)
    );
    performancesCopyYear = performancesCopyYear.performances.map(
      (perf) => {
        for (let thing of obj) {
          if (perf.symbol === thing.symbol) {
            perf["close"] = perf["close"] * thing.share;
          }
        }
        return perf;
      }
    );

    let newPerformancesYear = [];
    for (let i = 0; i < performancesCopyYear.length; i++) {
      let dateYearIndex = newPerformancesYear.findIndex(
        (perf) => perf.date === performancesCopyYear[i].date
      );

      if (dateYearIndex < 0) {
        newPerformancesYear.push({
          close: performancesCopyYear[i].close,
          date: performancesCopyYear[i].date,
        });
      } else {
        newPerformancesYear[dateYearIndex].close +=
          performancesCopyYear[i].close;
      }
    }

    let GainOrLossInPercentsYear = Math.round((((100 * newPerformancesYear[newPerformancesYear.length - 1].close) / newPerformancesYear[0].close) -100 ) * 100) / 100;
    const arrowDirectionYear = GainOrLossInPercentsYear > 0 ? arrowUp : arrowDown;
    const colorCodeYear = GainOrLossInPercentsYear > 0 ? "primary" : "danger";

    let minYear = Number.POSITIVE_INFINITY;
    let maxYear = Number.NEGATIVE_INFINITY;
    let tmpYear;
    for (let i = newPerformancesYear.length - 1; i >= 0; i--) {
      tmpYear = newPerformancesYear[i].close;
      if (tmpYear < minYear) minYear = tmpYear;
      if (tmpYear > maxYear) maxYear = tmpYear;
    }
    minYear = Math.round(minYear * 100) / 100;
    maxYear = Math.round(maxYear * 100) / 100;

    const compareMarketYear = GainOrLossInPercentsYear > GainOrLossInPercentsMarketYear ? `You are beating the market by ${Math.round(((GainOrLossInPercentsYear - GainOrLossInPercentsMarketYear)/(GainOrLossInPercentsMarketYear + 100)*100) * 100) / 100}% !` : `The market is beating you by ${Math.abs(Math.round(((GainOrLossInPercentsYear - GainOrLossInPercentsMarketYear)/(GainOrLossInPercentsMarketYear + 100)*100) * 100) / 100)}%`;
    const compareMarketMonth = GainOrLossInPercentsMonth > GainOrLossInPercentsMarketMonth ? `You are beating the market by ${Math.round(((GainOrLossInPercentsMonth - GainOrLossInPercentsMarketMonth)/(GainOrLossInPercentsMarketMonth + 100)*100) * 100) / 100}% !` : `The market is beating you by ${Math.abs(Math.round(((GainOrLossInPercentsMonth - GainOrLossInPercentsMarketMonth)/(GainOrLossInPercentsMarketMonth + 100)*100) * 100) / 100)}%`;
    const compareMarketDay = GainOrLossInPercentsDay > GainOrLossInPercentsMarketDay ? `You are beating the market by ${Math.round(((GainOrLossInPercentsDay - GainOrLossInPercentsMarketDay)/(GainOrLossInPercentsMarketDay + 100)*100) * 100) / 100}% !` : `The market is beating you by ${Math.abs(Math.round(((GainOrLossInPercentsDay - GainOrLossInPercentsMarketDay)/(GainOrLossInPercentsMarketDay + 100)*100) * 100) / 100)}%`;

    function numberWithCommas(x) {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    return (
      <>
        <div className="container-xl mt-2 mb-2 card shadow">
          <h2 className="text-primary font-weight-bold pt-2">
            Simulation Of The Current Portfolio Performance
          </h2>
          {showYear ? (
            <>
              <div className="mb-0 pt-0">
                <button
                  type="button"
                  className="btn btn-primary text-white mb-3 mt-3"
                  onClick={hideYear}
                >
                  Hide
                </button>
              </div>
              <h2 className="text-center">
                Over the year:
                &nbsp;&nbsp;
                <em className={`text-${colorCodeYear} fw-bolder`}>
                  {arrowDirectionYear}
                  {numberWithCommas(Math.abs(GainOrLossInPercentsYear))}%
                </em>
              </h2>
              <h4 className="text-center fst-italic">
                (min: ${numberWithCommas(Math.abs(minYear))} - max: ${numberWithCommas(Math.abs(maxYear))})
              </h4>
              <Chart2 performance={newPerformancesYear} period="Year" />
              <h2 className="text-center fst-italic">
                SP 500:
                &nbsp;
                <em className={`text-${colorCodeMarketYear} fw-bolder`}>
                  {arrowDirectionMarketYear}
                  {numberWithCommas(Math.abs(GainOrLossInPercentsMarketYear))}%
                </em>
                &nbsp; - {compareMarketYear}
              </h2>
            </>
          ) : (
            <>
              <div>
                <button
                  type="button"
                  className="btn btn-primary text-white mb-3 mt-3"
                  onClick={hideYear}
                >
                  Show Yearly Performances
                </button>
              </div>
            </>
          )}
          <hr />
          {showMonth ? (
            <>
              <div className="mb-0 pt-0">
                <button
                  type="button"
                  className="btn btn-primary text-white mb-3 mt-3"
                  onClick={hideMonth}
                >
                  Hide
                </button>
              </div>
              <h2 className="text-center">
                Over the month:
                &nbsp;&nbsp;
                <em className={`text-${colorCodeMonth} fw-bolder`}>
                  {arrowDirectionMonth}
                  {numberWithCommas(Math.abs(GainOrLossInPercentsMonth))}%
                </em>
              </h2>
              <h4 className="text-center fst-italic">
                (min: ${numberWithCommas(Math.abs(minMonth))} - max: ${numberWithCommas(Math.abs(maxMonth))})
              </h4>
              <Chart2 performance={newPerformancesMonth} period="Month" />
              <h2 className="text-center fst-italic">
                SP 500:
                &nbsp;
                <em className={`text-${colorCodeMarketMonth} fw-bolder`}>
                  {arrowDirectionMarketMonth}
                  {numberWithCommas(Math.abs(GainOrLossInPercentsMarketMonth))}%
                </em>
                &nbsp; - {compareMarketMonth}
              </h2>
            </>
          ) : (
            <>
              <div>
                <button
                  type="button"
                  className="btn btn-primary text-white mb-3 mt-3"
                  onClick={hideMonth}
                >
                  Show Monthly Performances
                </button>
              </div>
            </>
          )}
          <hr />
          {showDay ? (
            <>
              <div className="mb-0 pt-0">
                <button
                  type="button"
                  className="btn btn-primary text-white mb-3 mt-3"
                  onClick={hideDay}
                >
                  Hide
                </button>
              </div>
              <h2 className="text-center">
                During the day:
                &nbsp;&nbsp;
                <em className={`text-${colorCodeDay} fw-bolder`}>
                  {arrowDirectionDay}
                  {numberWithCommas(Math.abs(GainOrLossInPercentsDay))}%
                </em>
              </h2>
              <h4 className="text-center fst-italic">
                (min: ${numberWithCommas(Math.abs(minDay))} - max: ${numberWithCommas(Math.abs(maxDay))})
              </h4>
              <Chart2 performance={newPerformancesDay} period="Day" />
              <h2 className="text-center fst-italic">
                SP 500:
                &nbsp;
                <em className={`text-${colorCodeMarketDay} fw-bolder`}>
                  {arrowDirectionMarketDay}
                  {numberWithCommas(Math.abs(GainOrLossInPercentsMarketDay))}%
                </em>
                &nbsp; - {compareMarketDay}
              </h2>
              <p className="text-center fs-5">
                Note: The graph shows eastern time as it corresponds to the New York Stock Exchange.
              </p>
            </>
          ) : (
            <>
              <div>
                <button
                  type="button"
                  className="btn btn-primary text-white mb-3 mt-3"
                  onClick={hideDay}
                >
                  Show Daily Performances
                </button>
              </div>
            </>
          )}
        </div>
        <div className="clear"></div>
      </>
    );
  }
}

export default GetPerformances;
