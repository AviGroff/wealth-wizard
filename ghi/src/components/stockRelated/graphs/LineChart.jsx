import React from "react";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer,
} from "recharts";

function Chart2({ performance, period }) {
  const performanceCopy = JSON.parse(JSON.stringify(performance));

  let min = Number.POSITIVE_INFINITY;
  let max = Number.NEGATIVE_INFINITY;
  let tmp;
  for (let i = performance.length - 1; i >= 0; i--) {
    tmp = performance[i].close;
    if (tmp < min) min = tmp;
    if (tmp > max) max = tmp;
  }

  if (period !== "Day") {
    performanceCopy.map((perf) => {
      return (
        (perf["date"] = new Date(perf.date).toLocaleDateString()),
        (perf["close"] = perf["close"].toFixed(2)),
        (perf["Price ($)"] = perf["close"])
      );
    });
    return (
      <>
        <ResponsiveContainer width="100%" height={400}>
          <LineChart
            width={500}
            height={300}
            data={performanceCopy}
            margin={{
              top: 5,
              right: 30,
              left: 20,
              bottom: 5,
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="date" />
            <YAxis
              type="number"
              domain={[min - 0.01 * max, max + 0.01 * max]}
              tickCount={0}
            />
            <Tooltip />
            <Line
              type="monotone"
              dataKey="Price ($)"
              stroke="#12c574"
              strokeWidth={2}
              dot={false}
            />
          </LineChart>
        </ResponsiveContainer>
      </>
    );
  } else {
    performanceCopy.map((perf) => {
      return (
        (perf["date"] = new Date(perf.date).toLocaleTimeString([], { timeStyle: 'short' })),
        (perf["close"] = perf["close"].toFixed(2)),
        (perf["Price ($)"] = perf["close"])
      );
    });
    return (
      <>
        <ResponsiveContainer width="100%" height={400}>
          <LineChart
            width={500}
            height={300}
            data={performanceCopy}
            margin={{
              top: 5,
              right: 30,
              left: 20,
              bottom: 5,
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="date" />
            <YAxis
              type="number"
              domain={[min - 0.01 * max, max + 0.01 * max]}
              tickCount={0}
            />
            <Tooltip />
            <Line
              type="monotone"
              dataKey="Price ($)"
              stroke="#12c574"
              strokeWidth={2}
              dot={false}
            />
          </LineChart>
        </ResponsiveContainer>
      </>
    );
  }
}

export default Chart2;
