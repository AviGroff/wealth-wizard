import { useState } from "react";
import { useGetOrderHistoryQuery } from "../../app/portfolioApi";
import loadingGif from "../../media/gifs/newwizard.gif";

function OrderHistory() {
  const { data: stocks, isLoading } = useGetOrderHistoryQuery();
  const [filterTerm, setFilterTerm] = useState("");
  const [filterCategory, setFilterCategory] = useState("symbol");
  const diff = new Date().getTimezoneOffset()

  const getHistoryFiltered = () => {
    let reverseOrderedHistory = [...stocks.history].reverse();
    return reverseOrderedHistory.filter((stock) =>
      stock[filterCategory].includes(filterTerm)
    );
  };

  const handleFilterChange = (e) => {
    setFilterTerm(e.target.value.toUpperCase());
  };

  const handleFilterCategoryChange = (e) => {
    setFilterCategory(e.target.value);
  };

  const handlePlaceholder = filterCategory === "symbol" ? "Enter a symbol" : "YYYY-MM-DD";

  function formatDate(date) {
    var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

    if (month.length < 2) {
      month = '0' + month;
    }
    if (day.length < 2){
      day = '0' + day;
    }

    return [year, month, day].join('-');
  }

  if (isLoading) {
    return (
      <>
        <div className="container">
          <img
            className="p-40 col-30 position-absolute top-50 start-50 translate-middle img-responsive"
            src={loadingGif}
            alt="wait until the page loads"
          />
        </div>
      </>
    );
  } else {
    return (
      <>
        <div className="container-xl mt-2 mb-2 card shadow text-primary font-weight-bold">
          <h2>Order History</h2>

          <div className="row align-items-center">
            <div className="col align-items-center text-end">
              <div className="m-0 p-0">
                <div className="form-outline">
                  <select onChange={handleFilterCategoryChange}>
                    <option value="symbol">Filter by Symbol</option>
                    <option value="date">Filter by Date</option>
                  </select>
                </div>
              </div>
            </div>
            <div className="col">
              <div className="group p-2">
                <div className="col form-outline">
                  <input placeholder={handlePlaceholder} onChange={handleFilterChange} type="search" className="form-control w-50" />
                </div>
              </div>
            </div>
          </div>
          <table className="table table-striped">
            <thead>
              <tr>
                <th className="text-center">Name</th>
                <th className="text-center">Symbol</th>
                <th className="text-center">Shares</th>
                <th className="text-center">Price</th>
                <th className="text-center">Total</th>
                <th className="text-center">Date</th>
                <th className="text-center">Time</th>
                <th className="text-center">Transaction Type</th>
              </tr>
            </thead>
            <tbody className="bg-light">
              {getHistoryFiltered().map((stock) => {
                let corrected = new Date(new Date(stock.date).valueOf() - diff * 60000);
                return (
                  <tr className="bg-light align-middle" key={stock.id}>
                    <td className="text-center">{stock.name}</td>
                    <td className="text-center">{stock.symbol}</td>
                    <td className="text-center">{stock.shares}</td>
                    <td className="text-center">${stock.price}</td>
                    <td className="text-center">${stock.total}</td>
                    <td className="text-center">{formatDate(corrected.toLocaleDateString())}</td>
                    <td className="text-center">
                      {corrected.toLocaleTimeString([], { timeStyle: 'short' })}
                    </td>
                    <td className="text-center">
                      {stock.transaction_type}
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div >
        <div className="clear"></div>
      </>
    );
  }
}

export default OrderHistory;
