## Wealth Wizard

-Avi Groff
-Alix de Pannemaecker
-Jakob Schweter
-Matthew Yoakum

## Wealth Wizard – The premiere solution for stocks portfolio management!

## Design:

API design
Data model
GHI
Integrations

- [API design](docs/Endpoints.md)
- [Data model](docs/data-model.md)
- [GHI](docs/ghi.md)
- [Integrations](docs/integrations.md)

---

## Intended market:

We are targeting the general consumer wishing to hone their stock-management prowess. Our app is a sandbox aimed at allowing the user to practice their stock trading skills and expanding their knowledge about stocks without having to use real money.

---

## Functionality:

Upon making a new account, users are given $10,000 to play with. They can use this cash to buy (and later sell) any stock on the NYSE at their real-time price.

The home page contains explaination of how the website works, and stats on the technologies used during development which are shown via a percentage. The percentages were obtained via automatic calculation on gitlab.

The portfolio page allows the user to see their entire portfolio, including all of their stocks, amount of shares, current price, and total amount.
On this page users can also see their overall portfolio performance, and whether gains or losses have been made since creation of the account.
At the bottom of the page, related news is shown for each stock in the users portfolio.
On each individual stock in the portfolio, the user can click on the "Go to stock" button which takes them to a page showcasing that particular stock.

The individual stock page provides a graph showing that stock's performance over the past month. Click on the Buy or Sell button to bring up a modal to buy/sell the stock. Related news is shown at the bottom of the page.

The history page keeps track of each buy and sell transaction made, in chronological order. Searchable/filterable by stock symbol or date.

The statistics page contains three graphs showing the user's portfolio performance. The first shows the portfolio's positive/negative gains throughout the day.
The next extrapolates the user's current portfolio and gives a snapshot on how it would have performed for the past month.
The next extrapolates the user's current portfolio and gives a snapshot on how it would have performed for the past year.

---

## Project Initialization

To use this application on your local machine, please make sure to follow these steps:

Clone the repository

CD into the new project directory

Run `docker compose build`

Run `docker compose up`

Proceed to localhost:3000 in your browser and enjoy!
